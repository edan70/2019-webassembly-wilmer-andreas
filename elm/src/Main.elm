module Main exposing (..)
import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)



-- MAIN


main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL

type alias Model = 
    { jsFib : String
    , wasmFib : String
    }

init : Model
init =
    Model "" ""

fibWrapperJs : Maybe Int -> Int
fibWrapperJs str = 
    case str of
    Nothing -> fib 0
    Just i -> fib i

fibWrapperWasm : Maybe Int -> Int
fibWrapperWasm str = 
    case str of
    Nothing -> 0
    Just i -> 1

fib : Int -> Int
fib x =
    case x of
    0 -> 1
    1 -> 1
    _ -> fib (x-1) + fib (x-2)

-- UPDATE

type Msg = JsFib String | WasmFib String

update : Msg -> Model -> Model
update msg model =
  case msg of
    JsFib i ->
      {model | jsFib = i}

    WasmFib i ->
      {model | wasmFib = i}


-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ viewInput "text" "JavaScript" model.jsFib JsFib
    , div [] [ (text << String.fromInt << fibWrapperJs << String.toInt) model.jsFib]
    , viewInput "text" "Wasm" model.wasmFib WasmFib
    , div [] [ (text << String.fromInt << fibWrapperWasm << String.toInt) model.wasmFib]
    ]


viewInput : String -> String -> String -> (String -> msg) -> Html msg
viewInput t p v toMsg =
  input [ type_ t, placeholder p, value v, onInput toMsg ] []
