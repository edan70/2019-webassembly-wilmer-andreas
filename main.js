function readfunction(){
    var value = prompt("Enter an integer:");
    return parseInt(value);
  };
  var importObject = {imports: { print: arg => console.log(arg),
                                 read: readfunction}};
                                 
  WebAssembly.instantiateStreaming(fetch('main.wasm', {mode: 'no-cors'}), importObject)
  .then(obj => obj.instance.exports.main());
  