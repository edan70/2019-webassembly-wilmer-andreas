#!/usr/bin/env bash

java -jar webassembler.jar $1 > main.wat && 
	wat2wasm main.wat -o main.wasm &&
	echo "WASM-module created from file $1"
