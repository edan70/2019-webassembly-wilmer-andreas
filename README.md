# 2019-WebAssembly-wilmer-andreas

## Final release - prerequisites

The following must be done on the local machine to fully use the tool:

+ Install "WebAssembly Binary Toolkit": tool for conversion to- and from the .wat and .wasm formats (https://github.com/webassembly/wabt)
+ Install "Java 8 SDK"
+ Preparing firefox: open up the browser. Enter "about:config" into the URL field, press "I accept the risk".
In the search field, enter "privacy.file\_unique\_origin". In the list below, if the property is set to true,
right-click and press "toggle", so that the value of the property is set to "false".


## How to use

1. Install WABT from the prerequisites.
2. After having navigated to this folder from a terminal, run "./gradlew build" in order to build the file "webassembler.jar".
3. Run the command "java -jar webassembler.jar *your_simplic_file.in* > main.wat", creating a .wat-file with the human-readable web assembly from the simpli\_file given as the argument to webassembler.jar.
4. Run the command "wat2wasm main.wat -o main.wasm".
5. The included main.html- and main.js-files reference the previously generated .wasm-file. Start the program by double clicking main.html.
6. In order to see the output of the program, bring up the developer console in the browser (pressing F12 in Firefox), which contains the output of print-statements.

## Extra

The repository also contains:

+ _webcompile.sh_: shell script which performs step 3 and 4. Usage : _./webcompile.sh your_simplic_file.in_. Creates both _main.wat_ and then _main.wasm_.
+ _simplicexample.in_: Example SimpliC source code.


