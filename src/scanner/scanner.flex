package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
COMMENT = "//"[^\n]*
WhiteSpace = [ ] | \t | \f | \n | \r
ID = [a-zA-Z][a-zA-Z0-9]*
NUMERAL = 0|[1-9][0-9]*
%%

// discard whitespace information
{COMMENT}     { }
{WhiteSpace}  { }
// token definitions
"=="	        { return sym(Terminals.EQ);     }
("=>"|">=")		{ return sym(Terminals.GEQ);    }
">"		{ return sym(Terminals.GT);     }
("=<"|"<=")		{ return sym(Terminals.LEQ);    }
"<"		{ return sym(Terminals.LT);     }
"!="		{ return sym(Terminals.NEQ);    }
"("		{ return sym(Terminals.LPAR);   }
")"		{ return sym(Terminals.RPAR);   }
"{"		{ return sym(Terminals.LBRACE); }
"}"		{ return sym(Terminals.RBRACE); }
"="             { return sym(Terminals.ASSI);   }
";"             { return sym(Terminals.SEMIC);  }
","		{ return sym(Terminals.COMMA);  }
"+"		{ return sym(Terminals.ADD);    }
"-"		{ return sym(Terminals.SUB);    }
"/"		{ return sym(Terminals.DIV);    }
"*"		{ return sym(Terminals.MUL);    }
"%"		{ return sym(Terminals.MOD);    }
"int"		{ return sym(Terminals.INT);    }
"return"	{ return sym(Terminals.RETURN); }
"while"         { return sym(Terminals.WHILE);  }
"if"            { return sym(Terminals.IF);     }
"else"          { return sym(Terminals.ELSE);   }
"int"           { return sym(Terminals.INT);    }
"boolean"       { return sym(Terminals.BOOLEAN);}
{ID}            { return sym(Terminals.ID);     }
{NUMERAL}       { return sym(Terminals.NUM);    }
<<EOF>>         { return sym(Terminals.EOF);    }

/* error fallback */
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }
