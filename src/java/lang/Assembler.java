package lang;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import beaver.Parser.Exception;

import lang.ast.Program;
import lang.ast.Func;
import lang.ast.IdDecl;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.ast.ErrorMessage;



public class Assembler {
    public static Object DrAST_root_node; // Enable debugging with DrAST.

    public static void main(String[] args) {
        try {
            if (args.length != 1) {
                System.err.println(
                        "You must specify a source file on the command line!");
                printUsage();
                System.exit(1);
                return;
            }

            String filename = args[0];
            LangScanner scanner = new LangScanner(new FileReader(filename));
            LangParser parser = new LangParser();
            Program program = (Program) parser.parse(scanner);
            //program.prettyPrint(System.out);
            if (!program.errors().isEmpty()) {
                System.err.println();
                System.err.println("Errors: ");
                for (ErrorMessage e: program.errors()) {
                    System.err.println("- " + e);
                }
            }else{
                /*System.out.println("Function calls: ");
                for(Func f : program.getFuncList()){
                    for(FuncHelper fu : f.functionCalls()){
                        System.out.println(f.getFuncName().getID() + " -> " + fu);
                    }
                }
                System.out.println("Reachable: ");
                for(Func f : program.getFuncList()){
                    System.out.print(f.getFuncName().getID() + " {");
                    for(FuncHelper fu : f.reachable()){
                        System.out.print(fu.getFuncUse().getID() + ", ");
                    }
                    System.out.println("}");
                }*/
                program.genCode(System.out);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void printUsage() {
        System.err.println("Usage: Compiler FILE");
        System.err.println("    where FILE is the file to be compiled");
    }
}

