

aspect DeclarationAnalysis {
    syn boolean IdUse.isBadType();

    eq FuncUse.isBadType(){
     IdDecl d = decl();
     if(!d.isUnknown())
        return d.isVariable();
     return false;
    }
    eq VarUse.isBadType(){
        IdDecl d = decl();
        if(!d.isUnknown())
            return d.isFunction();
        return false;
    }

    inh boolean ASTNode.isFunction();
    inh boolean ASTNode.isVariable();

    syn boolean VarDecl.isFunction() = false;
    syn boolean VarDecl.isVariable() = true;

    syn boolean FuncDecl.isFunction() = true;
    syn boolean FuncDecl.isVariable() = false;

    eq Func.getFuncName().isVariable() = false;
    eq Func.getFuncName().isFunction() = true;

    eq Decl.getVarDecl().isVariable() = true;
    eq Decl.getVarDecl().isFunction() = false;
}

aspect TypeAnalysis {
	syn Type Expr.type();
	eq ArExpr.type() = intType();
	eq BoolExpr.type() = booleanType();
	eq Num.type() = intType();
	eq IdUse.type() = decl().type();
	syn Type IdDecl.type() = intType();
	eq Call.type() = getFuncUse().type();
	eq PrecExpr.type(){
	 //Debugger.debugPrint("Evaluating precedence expression on line " + getLine());
	 Type t = getExpr().type();
	 return t;
	}

	inh Type Expr.expectedType();
	eq While.getCond().expectedType() = booleanType();
	eq If.getCond().expectedType() = booleanType();
	eq Ret.getExpr().expectedType() = intType();
	eq Assi.getExpr().expectedType() = intType();
	eq Decl.getExpr().expectedType() = intType();
	eq CallStmt.getCall().expectedType() = intType();

	syn boolean Type.compatibleType(Type t){
		return this.equals(t);
	}

	syn boolean Assi.compatibleTypes() = getExpr().expectedType().compatibleType(getExpr().type());

	syn boolean Decl.compatibleTypes() {
	    //Debugger.debugPrint("Checking declaration on line " + getLine());
		if(hasExpr()) {
			return getExpr().expectedType().compatibleType(getExpr().type());
		}
		return true;
	}

	syn boolean While.compatibleTypes() = getCond().expectedType().compatibleType(getCond().type());

	syn boolean If.compatibleTypes() = getCond().expectedType().compatibleType(getCond().type());


	syn boolean IdDecl.variableAndFunction(){
	    IdDecl parent = lookup(getID());
	    if(!parent.isUnknown())
	        return parent.isFunction() && this.isVariable();
	    return false;
	}




	inh Func IdDecl.function();
	eq Func.getFuncName().function() = this;
	eq Decl.getVarDecl().function() = null;



	syn boolean Call.hasAllParam() = getFuncUse().decl().sameParams(this);

	syn boolean IdDecl.sameParams(Call c){
	    Func f = function();
	    if(f == null)
	        return true;
		if(f.getNumArguments() == c.getNumExpr()){
			return true;
		}
		return false;
	}

}
